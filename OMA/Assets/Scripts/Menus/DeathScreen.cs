﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathScreen : MonoBehaviour {

    public static bool ShowDeathScreen = false;
    public GameObject pauseMenuUI;
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Insert))
        {
            if (ShowDeathScreen)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
	}

    //TODO Korjaa ettei lyö kun painaa resume.
    public void Resume()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        ShowDeathScreen = false;
    }

    void Pause()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        ShowDeathScreen = true;
    }

    public void Restart()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        ShowDeathScreen = false;
        SceneManager.LoadScene(1);
    }

    public void QuitToMenu()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        ShowDeathScreen = false;
        SceneManager.LoadScene(0);
    }

    public void quitGame()
    {
        Debug.Log("Pressed quitGame in pause menu");
        Application.Quit();
    }
}
