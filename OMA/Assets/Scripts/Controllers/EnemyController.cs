﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour {

    public float lookRadius = 10f;
    [SerializeField] public float attackRadius = 2f;
    public Animator animator;

    private bool isAttacking = false;

    Transform target;
    NavMeshAgent agent;

    // Use this for initialization
    void Start() {
        target = PlayerManager.instance.player.transform;
        agent = GetComponent<NavMeshAgent>();
        //animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update() {
        float distance = Vector3.Distance(target.position, transform.position);

        if (!isAttacking)
        {
            if (distance <= lookRadius)
            {
                if (distance <= attackRadius)
                {
                    isAttacking = true;
                    Debug.Log("Enemy is attacking");
                    Invoke("StartAttack", 1f); 
                }
                else
                {
                    agent.SetDestination(target.position);
                }
            }
            else
            {
                //TODO idle animation
            }
        }
    }
    private void StartAttack()
    {
        animator.SetBool("Attacking", true);
        Invoke("StopAttack", .5f);
    }
    private void StopAttack(){
        isAttacking = false;

        animator.SetBool("Attacking", false);
        Debug.Log("Enemy attack ended");

        agent.SetDestination(target.position); //TODO Vihollinen ei käänny takaisin pelaajaa päin
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);

        Gizmos.DrawWireSphere(transform.position, attackRadius);
    }
}
