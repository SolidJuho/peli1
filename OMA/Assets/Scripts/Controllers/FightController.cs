﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FightController : MonoBehaviour
{
    Animator animator;
    bool isSwordAnimating = false;
    // Use this for initialization
    void Start()
    {
        animator = GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (!isSwordAnimating)
            {
                animator.enabled = true;
                isSwordAnimating = true;
                Invoke("AnimateSword", 1f);
            }
        }
    }

    void AnimateSword()
    {
        animator.enabled = false;
        isSwordAnimating = false;
    }
}